//////////////////////////////////////////////////////////////////////
// Bus Agent Config
//////////////////////////////////////////////////////////////////////
// Wait for signals test code
//
//////////////////////////////////////////////////////////////////////

`ifndef BUS_AGENT_CONFIG__SV

class bus_agent_config extends uvm_object;
   
   `uvm_object_utils(bus_agent_config)

   virtual jelly_bean_if vif;

   function new(string name = "bus_agent_config");
      super.new(name);
   endfunction : new

   //
   // Task: wait_for_reset
   //
   // This method waits for the end of reset
   task wait_for_reset;
      @(posedge vif.rst_n);
   endtask //

endclass : bus_agent_config

`endif //  `ifndef BUS_AGENT_CONFIG__SV

