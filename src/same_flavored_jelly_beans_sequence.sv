//------------------------------------------------------------------------------
// Class: same_flavored_jelly_beans_sequence
//   Sequence of transactions.
//------------------------------------------------------------------------------

`ifndef SAME_FLAVORED_JELLY_BEANS_SEQUENCE__SV

class same_flavored_jelly_beans_sequence extends uvm_sequence#( jelly_bean_transaction );
   rand int unsigned num_jelly_beans; // knob

   constraint num_jelly_beans_con { num_jelly_beans inside { [2:4] }; }

   function new( string name = "" );
      super.new( name );
   endfunction: new

   task body();
      jelly_bean_transaction     jb_tx;
      jelly_bean_types::flavor_e jb_flavor;

      jb_tx = jelly_bean_transaction::type_id::create
	      (
	       .name  ( "jb_tx" ),
               .contxt( get_full_name()));
      
      assert( jb_tx.randomize() );
      jb_flavor = jb_tx.flavor; // Can it be get as rand item without create jb_tx?

      repeat ( num_jelly_beans ) begin
         jb_tx = jelly_bean_transaction::type_id::create
		 (
		  .name  ( "jb_tx" ),
                  .contxt( get_full_name()));
	 
         start_item( jb_tx );
         assert( jb_tx.randomize() with { jb_tx.flavor == jb_flavor; } );
         finish_item( jb_tx );
      end
   endtask: body

   `uvm_object_utils_begin( same_flavored_jelly_beans_sequence )
      `uvm_field_int( num_jelly_beans, UVM_ALL_ON )
   `uvm_object_utils_end
endclass: same_flavored_jelly_beans_sequence

`endif //  `ifndef SAME_FLAVORED_JELLY_BEANS_SEQUENCE__SV
