//------------------------------------------------------------------------------
// Class: gift_boxed_jelly_beans_sequence
//   Sequence of sequences.
//------------------------------------------------------------------------------

`ifndef GITF_BOXED_JELLY_BEANS_SEQUENCE__SV

class gift_boxed_jelly_beans_sequence extends uvm_sequence#( jelly_bean_transaction );
   rand int unsigned num_jelly_bean_flavors; // knob

   constraint num_jelly_bean_flavors_con { num_jelly_bean_flavors inside { [2:3] }; }

   function new( string name = "" );
      super.new( name );
   endfunction: new

   task body();
      same_flavored_jelly_beans_sequence jb_seq;
      repeat ( num_jelly_bean_flavors ) begin
         jb_seq = same_flavored_jelly_beans_sequence::type_id::create
		  (
		   .name  ( "jb_seq" ),
                   .contxt( get_full_name()));
	 
         assert( jb_seq.randomize() );
         jb_seq.start( m_sequencer );  // Start another sequence
      end
   endtask: body

   `uvm_object_utils_begin( gift_boxed_jelly_beans_sequence )
      `uvm_field_int( num_jelly_bean_flavors, UVM_ALL_ON )
   `uvm_object_utils_end
endclass: gift_boxed_jelly_beans_sequence

`endif //  `ifndef GITF_BOXED_JELLY_BEANS_SEQUENCE__SV
