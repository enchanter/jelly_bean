//------------------------------------------------------------------------------
// Class: jelly_bean_driver
//------------------------------------------------------------------------------

`ifndef JELLY_BEAN_DRIVER__SV

class jelly_bean_driver extends uvm_driver#( jelly_bean_transaction );
   `uvm_component_utils( jelly_bean_driver )

   virtual jelly_bean_if  jb_if;
   bus_agent_config       m_cfg;
   jelly_bean_transaction jb_tx;
   
   function new( string name, uvm_component parent );
      super.new( name, parent );
   endfunction: new

   function void build_phase( uvm_phase phase );
      super.build_phase( phase );
   endfunction: build_phase

   task reset_sig;
      jb_if.master_cb.command    <= jelly_bean_types::NO_OP;
      jb_if.master_cb.color      <= jelly_bean_types::NO_COLOR;
      jb_if.master_cb.flavor     <= jelly_bean_types::NO_FLAVOR;
      jb_if.master_cb.sour       <= 0;
      jb_if.master_cb.sugar_free <= 0;
      
      m_cfg.wait_for_reset();
   endtask : reset_sig;
   

   task main_phase( uvm_phase phase );
      uvm_table_printer p = new;

      if (!uvm_config_db#(bus_agent_config)::get(null, get_full_name(), "config", m_cfg)) begin
	 `uvm_error("BODY", "bus_agent_config not found");
      end

      reset_sig;
      
      forever begin
         @jb_if.master_cb;
         jb_if.master_cb.command <= jelly_bean_types::NO_OP;
         jb_if.master_cb.color   <= jelly_bean_types::NO_COLOR;
         jb_if.master_cb.flavor  <= jelly_bean_types::NO_FLAVOR;
         
         seq_item_port.get_next_item( jb_tx );
	   `uvm_info("DRV",
                      { "Sequence item for taster.\n", jb_tx.sprint() }, UVM_MEDIUM);
         @jb_if.master_cb;
         jb_if.master_cb.command <= jb_tx.command;
         if ( jb_tx.command == jelly_bean_types::WRITE ) begin
            jb_if.master_cb.flavor       <= jb_tx.flavor;
            jb_if.master_cb.color        <= jb_tx.color;
            jb_if.master_cb.sugar_free   <= jb_tx.sugar_free;
            jb_if.master_cb.sour         <= jb_tx.sour;
         end
         seq_item_port.item_done();
      end
   endtask: main_phase
   
endclass: jelly_bean_driver

`endif //  `ifndef JELLY_BEAN_DRIVER__SV
