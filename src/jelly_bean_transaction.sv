//------------------------------------------------------------------------------
// Class: jelly_bean_transaction
//------------------------------------------------------------------------------

`ifndef JELLY_BEAN_TRANSACTION__SV

class jelly_bean_transaction extends uvm_sequence_item;
   rand jelly_bean_types::flavor_e  flavor;
   rand jelly_bean_types::color_e   color;
   rand bit                         sugar_free;
   rand bit                         sour;
   rand jelly_bean_types::command_e command;
   jelly_bean_types::taste_e        taste;  // Taste is a output from DUT. Does not need to be randomized. 

   constraint flavor_color_con {
      flavor != jelly_bean_types::NO_FLAVOR;
      flavor == jelly_bean_types::APPLE     -> color != jelly_bean_types::BLUE;
      flavor == jelly_bean_types::BLUEBERRY -> color == jelly_bean_types::BLUE;
   }

   function new( string name = "" );
      super.new( name );
   endfunction: new

   `uvm_object_utils_begin( jelly_bean_transaction )
      `uvm_field_enum( jelly_bean_types::flavor_e, flavor, UVM_ALL_ON )
      `uvm_field_enum( jelly_bean_types::color_e,  color,  UVM_ALL_ON )
      `uvm_field_int ( sugar_free,   UVM_ALL_ON )
      `uvm_field_int ( sour,         UVM_ALL_ON )
      `uvm_field_enum( jelly_bean_types::command_e, command, UVM_ALL_ON )
      `uvm_field_enum( jelly_bean_types::taste_e,   taste,   UVM_ALL_ON )
   `uvm_object_utils_end

   // do_record
   function void do_record(uvm_recorder recorder);
      super.do_record(recorder);  // to record any inherited data members
      `uvm_record_field("flavor", flavor.name())
      `uvm_record_field("color", color.name())
      `uvm_record_field("sugar_free", sugar_free)
      `uvm_record_field("sour", sour)
      `uvm_record_field("command", command.name())
      `uvm_record_field("taste", taste.name())
   endfunction : do_record
   
endclass: jelly_bean_transaction

`endif
