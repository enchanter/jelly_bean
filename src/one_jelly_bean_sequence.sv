//------------------------------------------------------------------------------
// Class: one_jelly_bean_sequence
//------------------------------------------------------------------------------

`ifndef ONE_JELLY_BEAN_SEQUENCE__SV

class one_jelly_bean_sequence extends uvm_sequence#( jelly_bean_transaction );
   `uvm_object_utils( one_jelly_bean_sequence )

   function new( string name = "" );
      super.new( name );
   endfunction: new

   task body();
      jelly_bean_transaction jb_tx;
      jb_tx = jelly_bean_transaction::type_id::create
	      (
	       .name  ( "jb_tx" ),
               .contxt( get_full_name()));
      
      start_item( jb_tx );
      assert( jb_tx.randomize() );
      finish_item( jb_tx );
   endtask: body
endclass: one_jelly_bean_sequence

`endif //  `ifndef ONE_JELLY_BEAN_SEQUENCE__SV
