class jelly_bean_recipe_test extends jelly_bean_test;
   `uvm_component_utils( jelly_bean_recipe_test )
 
   function new( string name, uvm_component parent );
      super.new( name, parent );
   endfunction: new
 
   task main_phase( uvm_phase phase );
      jelly_bean_recipe_virtual_sequence jb_vseq;
 
      phase.raise_objection( .obj( this ) );
      jb_vseq = jelly_bean_recipe_virtual_sequence::type_id::create( .name( "jb_vseq" ), .contxt( get_full_name() ) );
      jb_vseq.jb_seqr1 = jb_env.jb_agent1.jb_seqr;
      jb_vseq.jb_seqr2 = jb_env.jb_agent2.jb_seqr;
      assert( jb_vseq.randomize() );
      jb_vseq.start( .sequencer( null ) );
      #100ns ;
      phase.drop_objection( .obj( this ) );
   endtask: main_phase
endclass: jelly_bean_recipe_test